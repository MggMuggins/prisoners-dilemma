use std::collections::HashMap;

mod game;
mod strategies;

use game::run_game;
use strategies::STRATEGIES;

const NUM_TURNS: u32 = 7;
const ITERATIONS: u32 = 20000;

fn run_strategies(print_rounds: bool) -> HashMap<String, u32> {
    let mut results = HashMap::new();
    
    for (_i, (name1, strategy1)) in STRATEGIES.iter().enumerate() {
        let mut result_vec = vec![];
        
        for (_j, (name2, strategy2)) in STRATEGIES.iter().enumerate() {
            /* Only compute things once, this would make things more complicated
            // I don't care enough about the extra CPU cycles
            if i < j {
                continue;
            }*/
            
            let result = run_game(strategy1, strategy2, NUM_TURNS);
            
            if print_rounds {
                println!("{} vs {}:\n{}", name1, name2, result);
            }
            result_vec.push(result);
        }
        
        let mut total = 0;
        for result in result_vec.iter() {
            total += result.p1_score;
        }
        
        results.insert(name1.to_string(), total);
    }
    results
}

fn main() {
    println!("{} iterations; {} turns:\n", ITERATIONS, NUM_TURNS);
    /*
    let averages = run_strategies(true);
    
    for (key, val) in averages.iter() {
        println!("{} average: {}", key, val);
    }*/
    
    let mut all_scores = HashMap::new();
    
    for (name, _) in STRATEGIES.iter() {
        all_scores.insert(name, 0);
    }
    
    for _ in 0..ITERATIONS {
        let result = run_strategies(false);
        
        for (name, _) in STRATEGIES.iter() {
            all_scores.insert(name, result.get(&name.to_string()).unwrap() + all_scores.get(name).unwrap());
        }
    }
    
    let mut max = ("", 0);
    
    for (key, val) in all_scores.iter() {
        println!("{} average: {}", key, val / ITERATIONS);
        
        if val / ITERATIONS > max.1 {
            max = (key, *val / ITERATIONS);
        }
    }
    
    println!("\n{} is best: {}", max.0, max.1);
}
