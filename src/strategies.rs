use rand::random;

use crate::game::{Response, Strategy};

fn co_ops(r: &Response) -> bool {
    *r == Response::Cooperate
}

fn defects(r: &Response) -> bool {
    *r == Response::Defect
}

pub const STRATEGIES: [(&'static str, &Strategy); 14] = [
    ("theif", &|_, _| Response::Defect),
    
    ("friend", &|_, _| Response::Cooperate),
    //*
    ("fifty_fifty", &|_, _| {
        if random::<bool>() {
            Response::Cooperate
        } else {
            Response::Defect
        }
    }),
    //*/
    
    ("grudger", &|_, other_hist| {
        if other_hist.iter().any(defects) {
            Response::Defect
        } else {
            Response::Cooperate
        }
    }),
    
    ("soft-majority", &|_, other_hist| {
        let other_coops = other_hist.iter().cloned()
            .filter(co_ops)
            .count();
        let other_defects = other_hist.iter().cloned()
            .filter(defects)
            .count();
        
        if other_coops >= other_defects {
            Response::Cooperate
        } else {
            Response::Defect
        }
    }),
    
    ("tit-for-tat", &|_, other_hist| {
        let len = other_hist.len();
        if len == 0 {
            Response::Cooperate
        } else {
            other_hist[len - 1]
        }
    }),
    
    ("tit-for-two-tats", &|_, other_hist| {
        let should_defect = other_hist.iter()
            .rev()
            .take(2)
            .all(defects);
        
        if should_defect && other_hist.len() > 1 {
            Response::Defect
        } else {
            Response::Cooperate
        }
    }),
    
    // Essentially forgive the first infraction, then tit-for-tat
    ("forgiving-tit-for-tat", &|_my_hist, other_hist| {
        /*
        let len = other_hist.len();
        if len == 0 {
            Response::Cooperate
        } else {
            let other_defects = other_hist.iter().cloned()
                .filter(defects)
                .count();
            let my_defects = _my_hist.iter().cloned()
                .filter(defects)
                .count();
            
            if other_defects - my_defects >= 2 && other_defects > 1 {
                Response::Defect
            } else {
                Response::Cooperate
            }
        }*/
        let other_defects = other_hist
            .iter().cloned()
            .filter(defects)
            .count();
        
        if other_defects <= 1 {
            Response::Cooperate
        } else {
            other_hist[other_hist.len() - 1]
        }
    }),
    
    ("two-tits-for-tat", &|_, other_hist| {
        let should_defect = other_hist
            .iter()
            .rev()
            .take(2)
            .any(defects);
        
        if should_defect {
            Response::Defect
        } else {
            Response::Cooperate
        }
    }),
    
    ("three-tits-for-tat", &|_, other_hist| {
        let should_defect = other_hist
            .iter()
            .rev()
            .take(3)
            .any(defects);
        
        if should_defect {
            Response::Defect
        } else {
            Response::Cooperate
        }
    }),
    
    ("soft-grudger", &|_, other_hist| {
        let should_defect = other_hist
            .iter()
            .rev()
            .take(4)
            .any(defects);
        
        if should_defect {
            Response::Defect
        } else {
            Response::Cooperate
        }
    }),
    
    ("suspicious-tit-for-tat", &|_, other_hist| {
        let len = other_hist.len();
        if len == 0 {
            Response::Defect
        } else {
            other_hist[len - 1]
        }
    }),
    
    ("hard-majority", &|_, other_hist| {
        if other_hist.len() == 0 {
            return Response::Defect;
        }
        
        let other_coops = other_hist.iter().cloned()
            .filter(co_ops)
            .count();
        let other_defects = other_hist.iter().cloned()
            .filter(defects)
            .count();
        
        if other_defects >= other_coops {
            Response::Defect
        } else {
            Response::Cooperate
        }
    }),
    
    ("reverse-tit-for-tat", &|_, other_hist| {
        let len = other_hist.len();
        if len == 0 {
            Response::Defect
        } else {
            other_hist[len - 1].reverse()
        }
    }),
];
