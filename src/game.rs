use std::fmt;

use colored::*;

pub type Strategy = dyn Fn(&[Response], &[Response]) -> Response;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Response {
    Cooperate,
    Defect,
}

impl Response {
    pub fn reverse(self) -> Response {
        match self {
            Response::Cooperate => Response::Defect,
            Response::Defect => Response::Cooperate,
        }
    }
}

 impl fmt::Display for Response {
     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
         match self {
            Response::Cooperate => write!(f, "{}", "$$".green().bold()),
            Response::Defect => write!(f, "{}", "##".red().bold()),
         }
     }
 }

#[derive(Debug)]
pub struct GameResults {
    pub p1_history: Vec<Response>,
    pub p2_history: Vec<Response>,
    pub p1_score: u32,
    pub p2_score: u32,
}

impl fmt::Display for GameResults {
    fn fmt(&self, f: &mut fmt::Formatter, ) -> fmt::Result {
        writeln!(f, "{} to {}", self.p1_score, self.p2_score)?;
        
        for play in self.p1_history.iter() {
            write!(f, "{}  ", play)?;
        }
        writeln!(f)?;
        
        for play in self.p2_history.iter() {
            write!(f, "{}  ", play)?;
        }
        Ok(())
    }
}

pub fn run_game(p1: &Strategy, p2: &Strategy, turns: u32) -> GameResults {
    let (mut p1_score, mut p2_score) = (0, 0);
    let (mut p1_history, mut p2_history) = (vec![], vec![]);
    
    for _turn in 0..turns {
        // Important that the history of the strategy being
        //   called is the first arg (problems with the closure approach)
        let p1_result = p1(&p1_history, &p2_history);
        let p2_result = p2(&p2_history, &p1_history);
        
        match (p1_result, p2_result) {
            (Response::Cooperate, Response::Cooperate) => {
                p1_score += 3;
                p2_score += 3;
            },
            (Response::Cooperate, Response::Defect) => {
                p2_score += 5;
            },
            (Response::Defect, Response::Cooperate) => {
                p1_score += 5;
            },
            (Response::Defect, Response::Defect) => {
                p1_score += 1;
                p2_score += 1;
            }
        }
        
        p1_history.push(p1_result);
        p2_history.push(p2_result);
    }
    
    GameResults {
        p1_history,
        p2_history,
        p1_score,
        p2_score,
    }
}
